﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ItpilotTest.Models;
using ItpilotTest.IServices;
using Newtonsoft.Json;
using ItpilotTest.ViewModels;

namespace ItpilotTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWeatherService weatherService;
        public HomeController(IWeatherService weatherService)
        {
            this.weatherService = weatherService;
        }

        public IActionResult Index(WeatherDataViewModel weatherDataViewModel)
        {
            return View(weatherDataViewModel);
        }

        // get weathers from the api
        public JsonResult GetWeatherData()
        {
            return Json(weatherService.GetWeatherFromApi());
        }

        //Save the weatherdata recieved from the api and get all saved data  
        [HttpPost]
        public IActionResult SaveWeatherAndGetData(string weatherData = "")
        {
            if (!string.IsNullOrEmpty(weatherData))
            {
                //deserialize the string input to a list of WeatherData 
                List<WeatherData> content = JsonConvert.DeserializeObject<List<WeatherData>>(weatherData);
      
                if (content.Count > 0)
                {
                   var weatherDataViewModel = new WeatherDataViewModel()
                    {
                        WeatherDatas = content
                    };

                    //save the data to the DB and get them back
                    var data = weatherService.SaveWeatherDataAndGetData(weatherDataViewModel);
                    if (data.ToList().Count > 0)
                    {
                        // add the recieved data back to the weather view model collection object
                        weatherDataViewModel.WeatherDatas = data;
                        return PartialView("PartialViews/WeatherViewPartial", weatherDataViewModel);
                    }
                    return NotFound();
                }

                return NotFound();
            }
            return NotFound();
        }

        // add comment to the city selected
        public IActionResult AddComment(string cityName, string comment)
        {
            // check if the cityName is not empty
            if (!string.IsNullOrEmpty(cityName))
            {
               //send to update
               var commentAdded = weatherService.AddComment(cityName, comment);
                if (commentAdded)
                {
                    // return message;
                    var weatherDataViewModel = new WeatherDataViewModel()
                    {
                        WeatherDatas = weatherService.WeatherDatas,
                    };
                    return PartialView("PartialViews/WeatherViewPartial", weatherDataViewModel);
                }
                return NotFound();
            }

            return NotFound();
        }
             
    }
}
