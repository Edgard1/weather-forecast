﻿using ItpilotTest.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItpilotTest.DataAccess
{
    public class WeatherDbContext : DbContext
    {
        public WeatherDbContext(DbContextOptions<WeatherDbContext> options) : base (options)
        {

        }

        public DbSet<WeatherData> WeatherDatas { get; set; }
    }
}
