﻿using ItpilotTest.Models;
using ItpilotTest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItpilotTest.IServices
{
    public interface IWeatherService
    {
        Object GetWeatherFromApi();
        IEnumerable<WeatherData> WeatherDatas { get; }
        IEnumerable<WeatherData> SaveWeatherDataAndGetData(WeatherDataViewModel data);

        bool AddComment(string cityName, string comment);
    }
}
