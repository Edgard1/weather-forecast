﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ItpilotTest.Models
{
    public class WeatherData
    {
        public int WeatherDataId { get; set; }

        [Display(Name = "City")]
        [MaxLength(100)]
        public string City { get; set; }

        [MaxLength(50)]
        [Display(Name = "Weather")]
        public string Weather { get; set; }

        [MaxLength(100)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Temperature")]
        public double Temp { get; set; }


        [Display(Name = "Comments")]
        public string Comments { get; set; }

    }
}
