﻿using ItpilotTest.DataAccess;
using ItpilotTest.IServices;
using ItpilotTest.Models;
using ItpilotTest.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ItpilotTest.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly WeatherDbContext weatherDbContext;

        public WeatherService(WeatherDbContext weatherDbContext)
        {
            this.weatherDbContext = weatherDbContext;
        }

        // get all weatherData
        public IEnumerable<WeatherData> WeatherDatas
        {
            get => weatherDbContext.WeatherDatas;

        }

        // add comment to the city table
        public bool AddComment(string cityName, string comment)
        {           
            // get the city from the DB
            var thisCity = weatherDbContext.WeatherDatas.Where(x => x.City == cityName.Replace("-", " ")).FirstOrDefault();

            // check if this city is not null and add the comment to this table
            if (thisCity != null)
            {
                thisCity.Comments = comment;
                // save the context
                weatherDbContext.SaveChanges();
                return true;
            }
            return false;
        }

        // get data from the api and return it as jsonFormat
        public object GetWeatherFromApi()
        {
            string url = "https://samples.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=439d4b804bc8187953eb36d2a8c26a02";

            var client = new WebClient();
            var content = client.DownloadString(url); // download the data from the url
            var JContent = JsonConvert.DeserializeObject<Object>(content); // deserialize the content recieved from the url

            return JContent;

        }

        // save or update 
        public IEnumerable<WeatherData> SaveWeatherDataAndGetData(WeatherDataViewModel data)
        {
            //weatherDbContext.WeatherDatas.AddRange(data.WeatherDatas);
            foreach (var item in data.WeatherDatas)
            {
                // get this data item from the table
                var dataItem = weatherDbContext.WeatherDatas.Where(x => x.City == item.City).FirstOrDefault();
                // if this date does not exist save it to the table
                if (dataItem == null)
                {
                   // save the weatherData to the Database
                    weatherDbContext.Add(item);
                    weatherDbContext.SaveChanges();
                }
                else
                {
                    // update the data
                    dataItem.City = item.City;
                    dataItem.Weather = item.Weather;
                    dataItem.Description = item.Description;
                    dataItem.Temp = item.Temp;
                    weatherDbContext.Update(dataItem);
                    weatherDbContext.SaveChanges();
                }
            }

            // return the saved data
            return WeatherDatas;

        }
    }
}
