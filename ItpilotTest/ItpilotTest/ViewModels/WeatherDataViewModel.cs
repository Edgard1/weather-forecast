﻿using ItpilotTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ItpilotTest.ViewModels
{
    public class WeatherDataViewModel
    {
        public IEnumerable<WeatherData> WeatherDatas { get; set; }
    }
}
