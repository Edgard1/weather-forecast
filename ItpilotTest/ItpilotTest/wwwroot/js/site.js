﻿
var weather = (function () {
    return {
        CommentSubmit: function (e) {
            var thisCity = $(this).attr("id");
            var comment = $("." + thisCity).val();

            // call ajax if the comment is not empty
            if (comment != "") {

                $.ajax({
                    type: "POST",
                    url: "/Home/AddComment/?cityName=" + thisCity + "&comment=" + comment,
                    data: "",
                    success: function (response) {
                        $(".data-content").html(response);        
                    },
                    error: function (error) {
                        alert("request error");
                    },
                    beforeSend: function () {
                        $("div.spanner").addClass("show");

                    },
                    complete: function () {
                        $("div.spanner").removeClass("show");
                    }
                });
            }
            else
                alert("add a comment");
        },

        // remove comment
        RemoveCommentSubmit: function () {
            var thisCity = $(this).attr("id");
            var comment = $("." + thisCity).val();
            // call ajax if the comment is not empty
            if (comment != "") {
             
                comment = "";// clear the text field
                $.ajax({
                    type: "POST",
                    url: "/Home/AddComment/?cityName=" + thisCity + "&comment=" + comment,
                    data: "",
                    success: function (response) {
                        $(".data-content").html(response);   
                    },
                    error: function (error) {
                        alert("request error");
                    },
                    beforeSend: function () {
                        $("div.spanner").addClass("show");

                    },
                    complete: function () {
                        $("div.spanner").removeClass("show");
                    }
                });
            }
            else
                alert("can not delete");
        }

    }
})();


// save or update the data recieved from the api
var saveData = function (data) {
    $.ajax({
        type: "POST",      
        dataType: 'JSON',
        url: "/Home/SaveWeatherAndGetData",
        data: { weatherData: JSON.stringify(data) },
        success: function (response) {           
            $(".data-content").html(response);
        },
        error: function (error) {           
            $(".data-content").html(error.responseText);        
        },
        beforeSend: function () {
            $("div.spanner").addClass("show");
        },
        complete: function () {
            $("div.spanner").removeClass("show");
            // datatable options
            $('#dtTable').DataTable({
                "searching": false,
                columnDefs: [{
                    orderable: false,
                    targets: [1, 2, 4]
                }]
            });
            $('.dataTables_length').addClass('bs-select');
        }
    });
}


var dataPredictedCloud = [];
$(document).ready(function () {

    // get weather data from the api
    $.ajax({
        type: "GET",      
        url: "/Home/GetWeatherData",
        success: function (result) {           
             // loop the list and get only all the cities that have a predicted weather of clouds.
            $.each(result.list, function (index, data) {
                if (data.weather[0].main === "Clouds") {
                    // get the needed data in object format
                    var item = { cityId: data.id, city: data.name, weather: data.weather[0].main, description: data.weather[0].description, temp: data.main.temp };
                     //add the data object in the array object
                    dataPredictedCloud.push(item);
                }
            });
             // save the dataPredictedCloud to the DB if this object contains data
            if (dataPredictedCloud.length > 0) {              
                saveData(dataPredictedCloud);                         
            }          
        },
        error: function (err) {
            console.log(err);
        }, beforeSend: function () {
            $("div.spanner").addClass("show");
        },
        complete: function () {
            $("div.spanner").removeClass("show");
        }
    });

    
    $('body').on('click', '.btn', weather.CommentSubmit);
    $('body').on('click', '.remove', weather.RemoveCommentSubmit);
    
});